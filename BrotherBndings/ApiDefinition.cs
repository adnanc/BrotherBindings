﻿using System;
using CoreGraphics;
using Foundation;
using ObjCRuntime;

namespace BrotherBindings
{

    // @interface BRPtouchPrintInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface BRPtouchPrintInfo
    {
        // @property (copy, nonatomic) NSString * strPaperName;
        [Export("strPaperName")]
        string StrPaperName { get; set; }

        // @property (assign, nonatomic) unsigned long ulOption;
        [Export("ulOption")]
        nuint UlOption { get; set; }

        // @property (assign, nonatomic) int nPrintMode;
        [Export("nPrintMode")]
        int NPrintMode { get; set; }

        // @property (assign, nonatomic) double scaleValue;
        [Export("scaleValue")]
        double ScaleValue { get; set; }

        // @property (assign, nonatomic) int nDensity;
        [Export("nDensity")]
        int NDensity { get; set; }

        // @property (assign, nonatomic) int nOrientation;
        [Export("nOrientation")]
        int NOrientation { get; set; }

        // @property (assign, nonatomic) int nHalftone;
        [Export("nHalftone")]
        int NHalftone { get; set; }

        // @property (assign, nonatomic) int nHorizontalAlign;
        [Export("nHorizontalAlign")]
        int NHorizontalAlign { get; set; }

        // @property (assign, nonatomic) int nVerticalAlign;
        [Export("nVerticalAlign")]
        int NVerticalAlign { get; set; }

        // @property (assign, nonatomic) int nPaperAlign;
        [Export("nPaperAlign")]
        int NPaperAlign { get; set; }

        // @property (assign, nonatomic) int nExtFlag;
        [Export("nExtFlag")]
        int NExtFlag { get; set; }

        // @property (assign, nonatomic) int nAutoCutFlag;
        [Export("nAutoCutFlag")]
        int NAutoCutFlag { get; set; }

        // @property (assign, nonatomic) BOOL bEndcut;
        [Export("bEndcut")]
        bool BEndcut { get; set; }

        // @property (assign, nonatomic) int nAutoCutCopies;
        [Export("nAutoCutCopies")]
        int NAutoCutCopies { get; set; }

        // @property (assign, nonatomic) BOOL bSpecialTape;
        [Export("bSpecialTape")]
        bool BSpecialTape { get; set; }

        // @property (assign, nonatomic) BOOL bHalfCut;
        [Export("bHalfCut")]
        bool BHalfCut { get; set; }

        // @property (assign, nonatomic) int nTopMargin;
        [Export("nTopMargin")]
        int NTopMargin { get; set; }

        // @property (assign, nonatomic) int nLeftMargin;
        [Export("nLeftMargin")]
        int NLeftMargin { get; set; }

        // @property (assign, nonatomic) int nSpeed;
        [Export("nSpeed")]
        int NSpeed { get; set; }

        // @property (assign, nonatomic) BOOL bBidirection;
        [Export("bBidirection")]
        bool BBidirection { get; set; }

        // @property (assign, nonatomic) int nCustomLength;
        [Export("nCustomLength")]
        int NCustomLength { get; set; }

        // @property (assign, nonatomic) int nCustomWidth;
        [Export("nCustomWidth")]
        int NCustomWidth { get; set; }

        // @property (assign, nonatomic) int nCustomFeed;
        [Export("nCustomFeed")]
        int NCustomFeed { get; set; }

        // @property (copy, nonatomic) NSString * strSaveFilePath;
        [Export("strSaveFilePath")]
        string StrSaveFilePath { get; set; }

        // @property (assign, nonatomic) BOOL bOverWrite;
        [Export("bOverWrite")]
        bool BOverWrite { get; set; }

        // @property (assign, nonatomic) int nRollPrinterCase;
        [Export("nRollPrinterCase")]
        int NRollPrinterCase { get; set; }

        // @property (assign, nonatomic) BOOL bRotate180;
        [Export("bRotate180")]
        bool BRotate180 { get; set; }

        // @property (assign, nonatomic) BOOL bPeel;
        [Export("bPeel")]
        bool BPeel { get; set; }

        // @property (assign, nonatomic) BOOL bCutMark;
        [Export("bCutMark")]
        bool BCutMark { get; set; }

        // @property (assign, nonatomic) int nLabelMargine;
        [Export("nLabelMargine")]
        int NLabelMargine { get; set; }
    }

    // @interface BRPtouchPrinterData : NSObject
    [BaseType(typeof(NSObject))]
    interface BRPtouchPrinterData
    {
        // -(NSArray *)getPrinterList;
        [Export("getPrinterList")]
        NSObject[] PrinterList();
    }

    [Static]
    partial interface Constants
    {
        // extern NSString * BRWLanConnectBytesWrittenNotification;
        [Field("BRWLanConnectBytesWrittenNotification", "__Internal")]
        NSString BRWLanConnectBytesWrittenNotification { get; }

        // extern NSString * BRBluetoothSessionBytesWrittenNotification;
        [Field("BRBluetoothSessionBytesWrittenNotification", "__Internal")]
        NSString BRBluetoothSessionBytesWrittenNotification { get; }

        // extern NSString * BRPtouchPrinterKitMessageNotification;
        [Field("BRPtouchPrinterKitMessageNotification", "__Internal")]
        NSString BRPtouchPrinterKitMessageNotification { get; }

        // extern NSString *const BRBytesWrittenKey;
        [Field("BRBytesWrittenKey", "__Internal")]
        NSString BRBytesWrittenKey { get; }

        // extern NSString *const BRBytesToWriteKey;
        [Field("BRBytesToWriteKey", "__Internal")]
        NSString BRBytesToWriteKey { get; }

        // extern NSString *const BRMessageKey;
        [Field("BRMessageKey", "__Internal")]
        NSString BRMessageKey { get; }

        // extern NSString * BRDeviceDidConnectNotification;
        [Field("BRDeviceDidConnectNotification", "__Internal")]
        NSString BRDeviceDidConnectNotification { get; }

        // extern NSString * BRDeviceDidDisconnectNotification;
        [Field("BRDeviceDidDisconnectNotification", "__Internal")]
        NSString BRDeviceDidDisconnectNotification { get; }

        // extern NSString * BRDeviceKey;
        [Field("BRDeviceKey", "__Internal")]
        NSString BRDeviceKey { get; }
    }


    // @interface BRPtouchPrinter : NSObject <NSNetServiceBrowserDelegate, NSNetServiceDelegate>
    [BaseType(typeof(NSObject))]
    interface BRPtouchPrinter : INSNetServiceBrowserDelegate, INSNetServiceDelegate
    {
        // -(id)initWithPrinterName:(NSString *)strPrinterName;
        [Export("initWithPrinterName:")]
        IntPtr Constructor(string strPrinterName);

        // -(id)initWithPrinterName:(NSString *)strPrinterName interface:(CONNECTION_TYPE)type;
        [Export("initWithPrinterName:interface:")]
        IntPtr Constructor(string strPrinterName, ConnectionType type);

        // -(BOOL)setPrinterName:(NSString *)strPrinterName;
        [Export("setPrinterName:")]
        bool SetPrinterName(string strPrinterName);

        // -(void)setPrintInfo:(BRPtouchPrintInfo *)printInfo;
        [Export("setPrintInfo:")]
        void SetPrintInfo(BRPtouchPrintInfo printInfo);

        // -(BOOL)setCustomPaperFile:(NSString *)strFilePath;
        [Export("setCustomPaperFile:")]
        bool SetCustomPaperFile(string strFilePath);

        // -(BOOL)isPrinterReady;
        [Export("isPrinterReady")]
        bool IsPrinterReady();

        // -(int)getPTStatus:(PTSTATUSINFO *)status;
        [Export("getPTStatus:")]
        unsafe int GetPTStatus(PTSTATUSINFO status);

        // -(NSString *)getFirmVersion;
        [Export("getFirmVersion")]
        string FirmVersion();

        // -(BOOL)sendTemplateFile:(NSArray *)sendFileArray;
        [Export("sendTemplateFile:")]
        bool SendTemplateFile(NSObject[] sendFileArray);

        // -(BOOL)sendFirmwareFile:(NSArray *)sendFileArray;
        [Export("sendFirmwareFile:")]
        bool SendFirmwareFile(NSObject[] sendFileArray);

        // -(int)sendTemplate:(NSString *)sendtemplateFilePath connectionType:(CONNECTION_TYPE)type;
        [Export("sendTemplate:connectionType:")]
        int SendTemplate(string sendtemplateFilePath, ConnectionType type);

        // -(void)setIPAddress:(NSString *)strIP;
        [Export("setIPAddress:")]
        void SetIPAddress(string strIP);

        // -(void)setupForBluetoothDeviceWithSerialNumber:(NSString *)serialNumber;
        [Export("setupForBluetoothDeviceWithSerialNumber:")]
        void SetupForBluetoothDeviceWithSerialNumber(string serialNumber);

        // -(int)startPrint __attribute__((deprecated("")));
        [Export("startPrint")]
        int StartPrint();

        // -(BOOL)startCommunication;
        [Export("startCommunication")]
        bool StartCommunication();

        // -(void)endPrint __attribute__((deprecated("")));
        [Export("endPrint")]
        void EndPrint();

        // -(void)endCommunication;
        [Export("endCommunication")]
        void EndCommunication();

        // -(int)sendFile:(NSString *)filePath;
        [Export("sendFile:")]
        int SendFile(string filePath);

        // -(int)sendData:(NSData *)data;
        [Export("sendData:")]
        int SendData(NSData data);

        // -(int)sendFileEx:(NSString *)filePath;
        [Export("sendFileEx:")]
        int SendFileEx(string filePath);

        // -(int)sendDataEx:(NSData *)data;
        [Export("sendDataEx:")]
        int SendDataEx(NSData data);

        //// -(int)printPDFAtPath:(NSString *)pdfPath pages:(NSUInteger *)indexes length:(NSUInteger)length copy:(int)nCopy;
        //[Export("printPDFAtPath:pages:length:copy:")]
        //int PrintPDFAtPath(string pdfPath, nuint[] indexes, nuint length, int nCopy);

        // -(int)printImage:(CGImageRef)imageRef copy:(int)nCopy;
        [Export("printImage:copy:")]
        unsafe int PrintImage(CGImage imageRef, int nCopy);

        // -(int)cancelPrinting;
        [Export("cancelPrinting")]
        int CancelPrinting();

        // -(void)setInterface:(CONNECTION_TYPE)strInterface;
        [Export("setInterface:")]
        void SetInterface(ConnectionType strInterface);

        // -(int)setAutoConnectBluetooth:(BOOL)flag;
        [Export("setAutoConnectBluetooth:")]
        int SetAutoConnectBluetooth(bool flag);

        // -(int)isAutoConnectBluetooth;
        [Export("isAutoConnectBluetooth")]
        int IsAutoConnectBluetooth();
    }

    // @interface BRPtouchDeviceInfo : NSObject
    [BaseType(typeof(NSObject))]
    interface BRPtouchDeviceInfo
    {
        // @property (copy, nonatomic) NSString * strIPAddress;
        [Export("strIPAddress")]
        string StrIPAddress { get; set; }

        // @property (copy, nonatomic) NSString * strLocation;
        [Export("strLocation")]
        string StrLocation { get; set; }

        // @property (copy, nonatomic) NSString * strModelName;
        [Export("strModelName")]
        string StrModelName { get; set; }

        // @property (copy, nonatomic) NSString * strPrinterName;
        [Export("strPrinterName")]
        string StrPrinterName { get; set; }

        // @property (copy, nonatomic) NSString * strSerialNumber;
        [Export("strSerialNumber")]
        string StrSerialNumber { get; set; }

        // @property (copy, nonatomic) NSString * strNodeName;
        [Export("strNodeName")]
        string StrNodeName { get; set; }

        // @property (copy, nonatomic) NSString * strMACAddress;
        [Export("strMACAddress")]
        string StrMACAddress { get; set; }

        // -(NSString *)description;
        [Export("description")]
        string Description();
    }

    // @interface BRPtouchNetworkManager : NSObject <NSNetServiceBrowserDelegate, NSNetServiceDelegate>
    [BaseType(typeof(NSObject))]
    interface BRPtouchNetworkManager : INSNetServiceBrowserDelegate, INSNetServiceDelegate
    {
        // @property (retain, nonatomic) NSMutableArray * registeredPrinterNames;
        [Export("registeredPrinterNames", ArgumentSemantic.Retain)]
        NSMutableArray RegisteredPrinterNames { get; set; }

        // @property (assign, nonatomic) BOOL isEnableIPv6Search;
        [Export("isEnableIPv6Search")]
        bool IsEnableIPv6Search { get; set; }

        // -(int)startSearch:(int)searchTime;
        [Export("startSearch:")]
        int StartSearch(int searchTime);

        // -(NSArray *)getPrinterNetInfo;
        [Export("getPrinterNetInfo")]
        NSObject[] PrinterNetInfo();

        // -(BOOL)setPrinterNames:(NSArray *)strPrinterNames;
        [Export("setPrinterNames:")]
        bool SetPrinterNames(NSObject[] strPrinterNames);

        // -(BOOL)setPrinterName:(NSString *)strPrinterName;
        [Export("setPrinterName:")]
        bool SetPrinterName(string strPrinterName);

        // -(id)initWithPrinterNames:(NSArray *)strPrinterNames;
        [Export("initWithPrinterNames:")]
        IntPtr Constructor(NSObject[] strPrinterNames);

        // -(id)initWithPrinterName:(NSString *)strPrinterName;
        [Export("initWithPrinterName:")]
        IntPtr Constructor(string strPrinterName);

        [Wrap("WeakDelegate")]
        BRPtouchNetworkDelegate Delegate { get; set; }

        // @property (assign, nonatomic) id<BRPtouchNetworkDelegate> delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Assign)]
        NSObject WeakDelegate { get; set; }
    }

    // @protocol BRPtouchNetworkDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface BRPtouchNetworkDelegate
    {
        // @required -(void)didFinishSearch:(id)sender;
        [Abstract]
        [Export("didFinishSearch:")]
        void DidFinishSearch(NSObject sender);
    }

    // @interface BRPtouchBluetoothManager : NSObject
    [BaseType(typeof(NSObject))]
    interface BRPtouchBluetoothManager
    {
        // +(BRPtouchBluetoothManager *)sharedManager;
        [Static]
        [Export("sharedManager")]
        BRPtouchBluetoothManager SharedManager();

        // -(NSArray *)pairedDevices;
        [Export("pairedDevices")]
        NSObject[] PairedDevices();

        // -(void)registerForBRDeviceNotifications;
        [Export("registerForBRDeviceNotifications")]
        void RegisterForBRDeviceNotifications();

        // -(void)unregisterForBRDeviceNotifications;
        [Export("unregisterForBRDeviceNotifications")]
        void UnregisterForBRDeviceNotifications();

        // -(void)brShowBluetoothAccessoryPickerWithNameFilter:(NSPredicate *)predicate;
        [Export("brShowBluetoothAccessoryPickerWithNameFilter:")]
        void BrShowBluetoothAccessoryPickerWithNameFilter(NSPredicate predicate);
    }

    // @interface BRPtouchPrinterKit : NSObject
    [BaseType(typeof(NSObject))]
    interface BRPtouchPrinterKit
    {
    }
}