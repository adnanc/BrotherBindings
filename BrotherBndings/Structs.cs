﻿using System.Runtime.InteropServices;

namespace BrotherBindings 
{
    public struct PTSTATUSINFO
{
    public byte byHead;

    public byte bySize;

    public byte byBrotherCode;

    public byte bySeriesCode;

    public byte byModelCode;

    public byte byNationCode;

    public byte byFiller;

    public byte byFiller2;

    public byte byErrorInf;

    public byte byErrorInf2;

    public byte byMediaWidth;

    public byte byMediaType;

    public byte byColorNum;

    public byte byFont;

    public byte byJapanesFont;

    public byte byMode;

    public byte byDensity;

    public byte byMediaLength;

    public byte byStatusType;

    public byte byPhaseType;

    public byte byPhaseNoHi;

    public byte byPhaseNoLow;

    public byte byNoticeNo;

    public byte byExtByteNum;

    public byte byLabelColor;

    public byte byFontColor;

    public byte[] byHardWareSetting;

    public byte[] byNoUse;
}

    public enum ConnectionType : uint
{
    TypeWlan,
    TypeBluetooth,
    Error
}
}