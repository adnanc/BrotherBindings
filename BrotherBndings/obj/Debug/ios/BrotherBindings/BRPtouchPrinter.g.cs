//
// Auto-generated from generator.cs, do not edit
//
// We keep references to objects, so warning 414 is expected

#pragma warning disable 414

using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using UIKit;
using GLKit;
using Metal;
using MapKit;
using Photos;
using ModelIO;
using SceneKit;
using Contacts;
using Security;
using Messages;
using AudioUnit;
using CoreVideo;
using CoreMedia;
using QuickLook;
using CoreImage;
using SpriteKit;
using Foundation;
using CoreMotion;
using ObjCRuntime;
using AddressBook;
using MediaPlayer;
using GameplayKit;
using CoreGraphics;
using CoreLocation;
using AVFoundation;
using NewsstandKit;
using FileProvider;
using CoreAnimation;
using CoreFoundation;

namespace BrotherBindings {
	[Register("BRPtouchPrinter", true)]
	public unsafe partial class BRPtouchPrinter : NSObject, INSNetServiceBrowserDelegate, INSNetServiceDelegate {
		
		[CompilerGenerated]
		static readonly IntPtr class_ptr = Class.GetHandle ("BRPtouchPrinter");
		
		public override IntPtr ClassHandle { get { return class_ptr; } }
		
		[CompilerGenerated]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("init")]
		public BRPtouchPrinter () : base (NSObjectFlag.Empty)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSend (this.Handle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			} else {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSendSuper (this.SuperHandle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			}
		}

		[CompilerGenerated]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected BRPtouchPrinter (NSObjectFlag t) : base (t)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
		}

		[CompilerGenerated]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected internal BRPtouchPrinter (IntPtr handle) : base (handle)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
		}

		[Export ("initWithPrinterName:")]
		[CompilerGenerated]
		public BRPtouchPrinter (string strPrinterName)
			: base (NSObjectFlag.Empty)
		{
			if (strPrinterName == null)
				throw new ArgumentNullException ("strPrinterName");
			var nsstrPrinterName = NSString.CreateNative (strPrinterName);
			
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("initWithPrinterName:"), nsstrPrinterName), "initWithPrinterName:");
			} else {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("initWithPrinterName:"), nsstrPrinterName), "initWithPrinterName:");
			}
			NSString.ReleaseNative (nsstrPrinterName);
			
		}
		
		[Export ("initWithPrinterName:interface:")]
		[CompilerGenerated]
		public BRPtouchPrinter (string strPrinterName, ConnectionType type)
			: base (NSObjectFlag.Empty)
		{
			if (strPrinterName == null)
				throw new ArgumentNullException ("strPrinterName");
			var nsstrPrinterName = NSString.CreateNative (strPrinterName);
			
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSend_IntPtr_UInt32 (this.Handle, Selector.GetHandle ("initWithPrinterName:interface:"), nsstrPrinterName, (UInt32)type), "initWithPrinterName:interface:");
			} else {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSendSuper_IntPtr_UInt32 (this.SuperHandle, Selector.GetHandle ("initWithPrinterName:interface:"), nsstrPrinterName, (UInt32)type), "initWithPrinterName:interface:");
			}
			NSString.ReleaseNative (nsstrPrinterName);
			
		}
		
		[Export ("cancelPrinting")]
		[CompilerGenerated]
		public virtual int CancelPrinting ()
		{
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend (this.Handle, Selector.GetHandle ("cancelPrinting"));
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("cancelPrinting"));
			}
		}
		
		[Export ("endCommunication")]
		[CompilerGenerated]
		public virtual void EndCommunication ()
		{
			if (IsDirectBinding) {
				global::ApiDefinition.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("endCommunication"));
			} else {
				global::ApiDefinition.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("endCommunication"));
			}
		}
		
		[Export ("endPrint")]
		[CompilerGenerated]
		public virtual void EndPrint ()
		{
			if (IsDirectBinding) {
				global::ApiDefinition.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("endPrint"));
			} else {
				global::ApiDefinition.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("endPrint"));
			}
		}
		
		[Export ("getFirmVersion")]
		[CompilerGenerated]
		public virtual string FirmVersion ()
		{
			if (IsDirectBinding) {
				return NSString.FromHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSend (this.Handle, Selector.GetHandle ("getFirmVersion")));
			} else {
				return NSString.FromHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("getFirmVersion")));
			}
		}
		
		[Export ("getPTStatus:")]
		[CompilerGenerated]
		public virtual int GetPTStatus (PTSTATUSINFO status)
		{
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend_PTSTATUSINFO (this.Handle, Selector.GetHandle ("getPTStatus:"), status);
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper_PTSTATUSINFO (this.SuperHandle, Selector.GetHandle ("getPTStatus:"), status);
			}
		}
		
		[Export ("isAutoConnectBluetooth")]
		[CompilerGenerated]
		public virtual int IsAutoConnectBluetooth ()
		{
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend (this.Handle, Selector.GetHandle ("isAutoConnectBluetooth"));
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("isAutoConnectBluetooth"));
			}
		}
		
		[Export ("isPrinterReady")]
		[CompilerGenerated]
		public virtual bool IsPrinterReady ()
		{
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.bool_objc_msgSend (this.Handle, Selector.GetHandle ("isPrinterReady"));
			} else {
				return global::ApiDefinition.Messaging.bool_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("isPrinterReady"));
			}
		}
		
		[Export ("printImage:copy:")]
		[CompilerGenerated]
		public virtual int PrintImage (CGImage imageRef, int nCopy)
		{
			if (imageRef == null)
				throw new ArgumentNullException ("imageRef");
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend_IntPtr_int (this.Handle, Selector.GetHandle ("printImage:copy:"), imageRef.Handle, nCopy);
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper_IntPtr_int (this.SuperHandle, Selector.GetHandle ("printImage:copy:"), imageRef.Handle, nCopy);
			}
		}
		
		[Export ("sendData:")]
		[CompilerGenerated]
		public virtual int SendData (NSData data)
		{
			if (data == null)
				throw new ArgumentNullException ("data");
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("sendData:"), data.Handle);
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("sendData:"), data.Handle);
			}
		}
		
		[Export ("sendDataEx:")]
		[CompilerGenerated]
		public virtual int SendDataEx (NSData data)
		{
			if (data == null)
				throw new ArgumentNullException ("data");
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("sendDataEx:"), data.Handle);
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("sendDataEx:"), data.Handle);
			}
		}
		
		[Export ("sendFile:")]
		[CompilerGenerated]
		public virtual int SendFile (string filePath)
		{
			if (filePath == null)
				throw new ArgumentNullException ("filePath");
			var nsfilePath = NSString.CreateNative (filePath);
			
			int ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinition.Messaging.int_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("sendFile:"), nsfilePath);
			} else {
				ret = global::ApiDefinition.Messaging.int_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("sendFile:"), nsfilePath);
			}
			NSString.ReleaseNative (nsfilePath);
			
			return ret;
		}
		
		[Export ("sendFileEx:")]
		[CompilerGenerated]
		public virtual int SendFileEx (string filePath)
		{
			if (filePath == null)
				throw new ArgumentNullException ("filePath");
			var nsfilePath = NSString.CreateNative (filePath);
			
			int ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinition.Messaging.int_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("sendFileEx:"), nsfilePath);
			} else {
				ret = global::ApiDefinition.Messaging.int_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("sendFileEx:"), nsfilePath);
			}
			NSString.ReleaseNative (nsfilePath);
			
			return ret;
		}
		
		[Export ("sendFirmwareFile:")]
		[CompilerGenerated]
		public virtual bool SendFirmwareFile (NSObject[] sendFileArray)
		{
			if (sendFileArray == null)
				throw new ArgumentNullException ("sendFileArray");
			var nsa_sendFileArray = NSArray.FromNSObjects (sendFileArray);
			
			bool ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("sendFirmwareFile:"), nsa_sendFileArray.Handle);
			} else {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("sendFirmwareFile:"), nsa_sendFileArray.Handle);
			}
			nsa_sendFileArray.Dispose ();
			
			return ret;
		}
		
		[Export ("sendTemplate:connectionType:")]
		[CompilerGenerated]
		public virtual int SendTemplate (string sendtemplateFilePath, ConnectionType type)
		{
			if (sendtemplateFilePath == null)
				throw new ArgumentNullException ("sendtemplateFilePath");
			var nssendtemplateFilePath = NSString.CreateNative (sendtemplateFilePath);
			
			int ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinition.Messaging.int_objc_msgSend_IntPtr_UInt32 (this.Handle, Selector.GetHandle ("sendTemplate:connectionType:"), nssendtemplateFilePath, (UInt32)type);
			} else {
				ret = global::ApiDefinition.Messaging.int_objc_msgSendSuper_IntPtr_UInt32 (this.SuperHandle, Selector.GetHandle ("sendTemplate:connectionType:"), nssendtemplateFilePath, (UInt32)type);
			}
			NSString.ReleaseNative (nssendtemplateFilePath);
			
			return ret;
		}
		
		[Export ("sendTemplateFile:")]
		[CompilerGenerated]
		public virtual bool SendTemplateFile (NSObject[] sendFileArray)
		{
			if (sendFileArray == null)
				throw new ArgumentNullException ("sendFileArray");
			var nsa_sendFileArray = NSArray.FromNSObjects (sendFileArray);
			
			bool ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("sendTemplateFile:"), nsa_sendFileArray.Handle);
			} else {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("sendTemplateFile:"), nsa_sendFileArray.Handle);
			}
			nsa_sendFileArray.Dispose ();
			
			return ret;
		}
		
		[Export ("setAutoConnectBluetooth:")]
		[CompilerGenerated]
		public virtual int SetAutoConnectBluetooth (bool flag)
		{
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend_bool (this.Handle, Selector.GetHandle ("setAutoConnectBluetooth:"), flag);
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper_bool (this.SuperHandle, Selector.GetHandle ("setAutoConnectBluetooth:"), flag);
			}
		}
		
		[Export ("setCustomPaperFile:")]
		[CompilerGenerated]
		public virtual bool SetCustomPaperFile (string strFilePath)
		{
			if (strFilePath == null)
				throw new ArgumentNullException ("strFilePath");
			var nsstrFilePath = NSString.CreateNative (strFilePath);
			
			bool ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("setCustomPaperFile:"), nsstrFilePath);
			} else {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("setCustomPaperFile:"), nsstrFilePath);
			}
			NSString.ReleaseNative (nsstrFilePath);
			
			return ret;
		}
		
		[Export ("setIPAddress:")]
		[CompilerGenerated]
		public virtual void SetIPAddress (string strIP)
		{
			if (strIP == null)
				throw new ArgumentNullException ("strIP");
			var nsstrIP = NSString.CreateNative (strIP);
			
			if (IsDirectBinding) {
				global::ApiDefinition.Messaging.void_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("setIPAddress:"), nsstrIP);
			} else {
				global::ApiDefinition.Messaging.void_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("setIPAddress:"), nsstrIP);
			}
			NSString.ReleaseNative (nsstrIP);
			
		}
		
		[Export ("setInterface:")]
		[CompilerGenerated]
		public virtual void SetInterface (ConnectionType strInterface)
		{
			if (IsDirectBinding) {
				global::ApiDefinition.Messaging.void_objc_msgSend_UInt32 (this.Handle, Selector.GetHandle ("setInterface:"), (UInt32)strInterface);
			} else {
				global::ApiDefinition.Messaging.void_objc_msgSendSuper_UInt32 (this.SuperHandle, Selector.GetHandle ("setInterface:"), (UInt32)strInterface);
			}
		}
		
		[Export ("setPrintInfo:")]
		[CompilerGenerated]
		public virtual void SetPrintInfo (BRPtouchPrintInfo printInfo)
		{
			if (printInfo == null)
				throw new ArgumentNullException ("printInfo");
			if (IsDirectBinding) {
				global::ApiDefinition.Messaging.void_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("setPrintInfo:"), printInfo.Handle);
			} else {
				global::ApiDefinition.Messaging.void_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("setPrintInfo:"), printInfo.Handle);
			}
		}
		
		[Export ("setPrinterName:")]
		[CompilerGenerated]
		public virtual bool SetPrinterName (string strPrinterName)
		{
			if (strPrinterName == null)
				throw new ArgumentNullException ("strPrinterName");
			var nsstrPrinterName = NSString.CreateNative (strPrinterName);
			
			bool ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("setPrinterName:"), nsstrPrinterName);
			} else {
				ret = global::ApiDefinition.Messaging.bool_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("setPrinterName:"), nsstrPrinterName);
			}
			NSString.ReleaseNative (nsstrPrinterName);
			
			return ret;
		}
		
		[Export ("setupForBluetoothDeviceWithSerialNumber:")]
		[CompilerGenerated]
		public virtual void SetupForBluetoothDeviceWithSerialNumber (string serialNumber)
		{
			if (serialNumber == null)
				throw new ArgumentNullException ("serialNumber");
			var nsserialNumber = NSString.CreateNative (serialNumber);
			
			if (IsDirectBinding) {
				global::ApiDefinition.Messaging.void_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("setupForBluetoothDeviceWithSerialNumber:"), nsserialNumber);
			} else {
				global::ApiDefinition.Messaging.void_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("setupForBluetoothDeviceWithSerialNumber:"), nsserialNumber);
			}
			NSString.ReleaseNative (nsserialNumber);
			
		}
		
		[Export ("startCommunication")]
		[CompilerGenerated]
		public virtual bool StartCommunication ()
		{
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.bool_objc_msgSend (this.Handle, Selector.GetHandle ("startCommunication"));
			} else {
				return global::ApiDefinition.Messaging.bool_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("startCommunication"));
			}
		}
		
		[Export ("startPrint")]
		[CompilerGenerated]
		public virtual int StartPrint ()
		{
			if (IsDirectBinding) {
				return global::ApiDefinition.Messaging.int_objc_msgSend (this.Handle, Selector.GetHandle ("startPrint"));
			} else {
				return global::ApiDefinition.Messaging.int_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("startPrint"));
			}
		}
		
	} /* class BRPtouchPrinter */
}
