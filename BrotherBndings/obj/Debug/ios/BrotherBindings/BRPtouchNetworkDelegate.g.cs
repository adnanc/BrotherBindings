//
// Auto-generated from generator.cs, do not edit
//
// We keep references to objects, so warning 414 is expected

#pragma warning disable 414

using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using UIKit;
using GLKit;
using Metal;
using MapKit;
using Photos;
using ModelIO;
using SceneKit;
using Contacts;
using Security;
using Messages;
using AudioUnit;
using CoreVideo;
using CoreMedia;
using QuickLook;
using CoreImage;
using SpriteKit;
using Foundation;
using CoreMotion;
using ObjCRuntime;
using AddressBook;
using MediaPlayer;
using GameplayKit;
using CoreGraphics;
using CoreLocation;
using AVFoundation;
using NewsstandKit;
using FileProvider;
using CoreAnimation;
using CoreFoundation;

namespace BrotherBindings {
	[Protocol (Name = "BRPtouchNetworkDelegate", WrapperType = typeof (BRPtouchNetworkDelegateWrapper))]
	[ProtocolMember (IsRequired = true, IsProperty = false, IsStatic = false, Name = "DidFinishSearch", Selector = "didFinishSearch:", ParameterType = new Type [] { typeof (NSObject) }, ParameterByRef = new bool [] { false })]
	public interface IBRPtouchNetworkDelegate : INativeObject, IDisposable
	{
		[CompilerGenerated]
		[Export ("didFinishSearch:")]
		[Preserve (Conditional = true)]
		void DidFinishSearch (NSObject sender);
		
	}
	
	internal sealed class BRPtouchNetworkDelegateWrapper : BaseWrapper, IBRPtouchNetworkDelegate {
		[Preserve (Conditional = true)]
		public BRPtouchNetworkDelegateWrapper (IntPtr handle, bool owns)
			: base (handle, owns)
		{
		}
		
		[Export ("didFinishSearch:")]
		[CompilerGenerated]
		public void DidFinishSearch (NSObject sender)
		{
			if (sender == null)
				throw new ArgumentNullException ("sender");
			global::ApiDefinition.Messaging.void_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("didFinishSearch:"), sender.Handle);
		}
		
	}
}
namespace BrotherBindings {
	[Protocol]
	[Register("BRPtouchNetworkDelegate", false)]
	[Model]
	public unsafe abstract partial class BRPtouchNetworkDelegate : NSObject, IBRPtouchNetworkDelegate {
		
		[CompilerGenerated]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("init")]
		protected BRPtouchNetworkDelegate () : base (NSObjectFlag.Empty)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSend (this.Handle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			} else {
				InitializeHandle (global::ApiDefinition.Messaging.IntPtr_objc_msgSendSuper (this.SuperHandle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			}
		}

		[CompilerGenerated]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected BRPtouchNetworkDelegate (NSObjectFlag t) : base (t)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
		}

		[CompilerGenerated]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected internal BRPtouchNetworkDelegate (IntPtr handle) : base (handle)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinition.Messaging.this_assembly;
		}

		[Export ("didFinishSearch:")]
		[CompilerGenerated]
		public abstract void DidFinishSearch (NSObject sender);
	} /* class BRPtouchNetworkDelegate */
}
