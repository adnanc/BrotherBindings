//
// Auto-generated from generator.cs, do not edit
//
// We keep references to objects, so warning 414 is expected

#pragma warning disable 414

using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using UIKit;
using GLKit;
using Metal;
using MapKit;
using Photos;
using ModelIO;
using SceneKit;
using Contacts;
using Security;
using Messages;
using AudioUnit;
using CoreVideo;
using CoreMedia;
using QuickLook;
using CoreImage;
using SpriteKit;
using Foundation;
using CoreMotion;
using ObjCRuntime;
using AddressBook;
using MediaPlayer;
using GameplayKit;
using CoreGraphics;
using CoreLocation;
using AVFoundation;
using NewsstandKit;
using FileProvider;
using CoreAnimation;
using CoreFoundation;

namespace BrotherBindings {
	public unsafe static partial class Constants  {
		
		[CompilerGenerated]
		static NSString _BRBluetoothSessionBytesWrittenNotification;
		[Field ("BRBluetoothSessionBytesWrittenNotification",  "__Internal")]
		public static NSString BRBluetoothSessionBytesWrittenNotification {
			get {
				if (_BRBluetoothSessionBytesWrittenNotification == null)
					_BRBluetoothSessionBytesWrittenNotification = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRBluetoothSessionBytesWrittenNotification");
				return _BRBluetoothSessionBytesWrittenNotification;
			}
		}
		[CompilerGenerated]
		static NSString _BRBytesToWriteKey;
		[Field ("BRBytesToWriteKey",  "__Internal")]
		public static NSString BRBytesToWriteKey {
			get {
				if (_BRBytesToWriteKey == null)
					_BRBytesToWriteKey = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRBytesToWriteKey");
				return _BRBytesToWriteKey;
			}
		}
		[CompilerGenerated]
		static NSString _BRBytesWrittenKey;
		[Field ("BRBytesWrittenKey",  "__Internal")]
		public static NSString BRBytesWrittenKey {
			get {
				if (_BRBytesWrittenKey == null)
					_BRBytesWrittenKey = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRBytesWrittenKey");
				return _BRBytesWrittenKey;
			}
		}
		[CompilerGenerated]
		static NSString _BRDeviceDidConnectNotification;
		[Field ("BRDeviceDidConnectNotification",  "__Internal")]
		public static NSString BRDeviceDidConnectNotification {
			get {
				if (_BRDeviceDidConnectNotification == null)
					_BRDeviceDidConnectNotification = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRDeviceDidConnectNotification");
				return _BRDeviceDidConnectNotification;
			}
		}
		[CompilerGenerated]
		static NSString _BRDeviceDidDisconnectNotification;
		[Field ("BRDeviceDidDisconnectNotification",  "__Internal")]
		public static NSString BRDeviceDidDisconnectNotification {
			get {
				if (_BRDeviceDidDisconnectNotification == null)
					_BRDeviceDidDisconnectNotification = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRDeviceDidDisconnectNotification");
				return _BRDeviceDidDisconnectNotification;
			}
		}
		[CompilerGenerated]
		static NSString _BRDeviceKey;
		[Field ("BRDeviceKey",  "__Internal")]
		public static NSString BRDeviceKey {
			get {
				if (_BRDeviceKey == null)
					_BRDeviceKey = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRDeviceKey");
				return _BRDeviceKey;
			}
		}
		[CompilerGenerated]
		static NSString _BRMessageKey;
		[Field ("BRMessageKey",  "__Internal")]
		public static NSString BRMessageKey {
			get {
				if (_BRMessageKey == null)
					_BRMessageKey = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRMessageKey");
				return _BRMessageKey;
			}
		}
		[CompilerGenerated]
		static NSString _BRPtouchPrinterKitMessageNotification;
		[Field ("BRPtouchPrinterKitMessageNotification",  "__Internal")]
		public static NSString BRPtouchPrinterKitMessageNotification {
			get {
				if (_BRPtouchPrinterKitMessageNotification == null)
					_BRPtouchPrinterKitMessageNotification = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRPtouchPrinterKitMessageNotification");
				return _BRPtouchPrinterKitMessageNotification;
			}
		}
		[CompilerGenerated]
		static NSString _BRWLanConnectBytesWrittenNotification;
		[Field ("BRWLanConnectBytesWrittenNotification",  "__Internal")]
		public static NSString BRWLanConnectBytesWrittenNotification {
			get {
				if (_BRWLanConnectBytesWrittenNotification == null)
					_BRWLanConnectBytesWrittenNotification = Dlfcn.GetStringConstant (Libraries.__Internal.Handle, "BRWLanConnectBytesWrittenNotification");
				return _BRWLanConnectBytesWrittenNotification;
			}
		}
	} /* class Constants */
}
